﻿using UnityEngine;
using System.Collections;

namespace TapRPG.Core.States
{
	public interface IPausable 
	{
		void ApplicationHasGoneIntoBackground();
	}
}