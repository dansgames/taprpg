﻿using UnityEngine;
using System.Collections;

namespace TapRPG.Core.States
{
	public interface IState
	{
		void OnEnter();
		void OnExit();
		bool CanTransition();
		IState NextState();
	}
}