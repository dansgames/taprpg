﻿using UnityEngine;
using System.Collections;

namespace TapRPG.Core.States
{
	public interface IUIState
	{
		void AnimateOn();
		void AnimateOff();
	}
}