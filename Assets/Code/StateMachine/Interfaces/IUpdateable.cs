﻿using UnityEngine;
using System.Collections;

namespace TapRPG.Core.States
{
	public interface IUpdateable 
	{
		void Update();
		void LateUpdate();
		void FixedUpdate();
	}
}