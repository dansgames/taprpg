﻿using UnityEngine;
using System.Collections;

namespace TapRPG.Core.States
{
	public interface IDoesTick
	{
		void OnTick();
	}
}