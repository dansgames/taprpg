﻿using UnityEngine;
using System.Collections;
using TapRPG.Core.States;
using System.Collections.Generic;
using System;

namespace TapRPG.Core {
	public sealed class StateMachine : MonoBehaviour
	{
		#region delegates
		public static Action<IState> StateChanged = delegate {};
		#endregion
		
		#region fields
		private IState _currentState;

		private float _timeOfLastTick = Time.time;
		#endregion
		
		#region properties
		public IState currentState
		{
			get { return _currentState; }
			private set { _currentState = value; }
		}
		#endregion
		
		#region methods
		void Update()
		{
			if(currentState != default(IState))
			{
				if(currentState is IUpdateable)
				{
					(currentState as IUpdateable).Update();
				}
				
				if(currentState.CanTransition())
				{
					var nextState = currentState.NextState();
					ChangeState(nextState);
				}

				if (Time.time > _timeOfLastTick + 1f) {

					_timeOfLastTick = Time.time;

					if(currentState is IDoesTick)
					{
						(currentState as IDoesTick).OnTick();
					}
				}
			}
		}
		
		public void ChangeState(IState nextState)
		{
			if(currentState != default(IState))
			{
				Debug.Log ("<color=yellow> SM- Exiting: " + currentState + "</color>");
				currentState.OnExit();
			}
			currentState = nextState;
			Debug.Log ("<color=green> SM- Entering: " + currentState + "</color>");
			currentState.OnEnter();
			
			StateChanged(currentState);
		}
		
		private void OnApplicationPause(bool paused)
		{
			if(!paused)
			{
				return;
			}
			
			if(currentState is IPausable)
			{
				(currentState as IPausable).ApplicationHasGoneIntoBackground();
			}
		}
#endregion
	}
}