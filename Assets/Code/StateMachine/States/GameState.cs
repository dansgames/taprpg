﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace TapRPG.Core.States
{
	public sealed class GameState : IState, IPausable, IUpdateable
	{
		#region private fields

		private Player _player;
		private LevelBuilder _levelBuilder;

		private bool _bFinished = false;

		#endregion

		public GameState() {
			
			GameObject player = GameObject.Instantiate (Resources.Load(ResourceDefs.kGAME_PlayerSuit + "Default")) as GameObject;
			_player = player.GetComponent<Player> ();
			
			GameObject levelBuilder = GameObject.Instantiate (Resources.Load(ResourceDefs.kGAME_LevelBuilderPrefab)) as GameObject;
			_levelBuilder = levelBuilder.GetComponent<LevelBuilder> ();

			if (TestDefs.kBuildTestLevelOnStart) {
				_levelBuilder.CreateLevel (TestDefs.kTestLevelType, TestDefs.kTestLevelDifficulty);
			}
		}

		#region IUpdateable Method

		public void Update() {
			if (_player.IsDead()) {

			}
		}

		public void LateUpdate() {}

		public void FixedUpdate() {}

		#endregion

		#region IState Methods

		public void OnEnter() {
			_player.ToggleMove (true);
		}

		public void OnExit() {
			GameObject.DestroyImmediate (_player.gameObject);
			GameObject.DestroyImmediate (_levelBuilder.gameObject);
		}

		public bool CanTransition() {
			return _bFinished;
		}

		public IState NextState() {
			return new TitleState();
		}

		#endregion

		#region IPausable methods

		public void ApplicationHasGoneIntoBackground() {
			_bFinished = true;
		}

		#endregion

		#region private methods

		private void Quit() {
			_bFinished = true;
		}

		#endregion
	}
}
