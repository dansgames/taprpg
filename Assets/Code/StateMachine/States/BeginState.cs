﻿using UnityEngine;
using System.Collections;

namespace TapRPG.Core.States
{
	public sealed class BeginState : IState, IUIState, IUpdateable
	{
		#region private variables

		private GameObject _flgTitleSreenPrefabObject;

		private float _timeElapsed = 0f;
		private float _maxTimeOnScreen = 3f;

		#endregion

		#region State Implementation

		// IState
		public void OnEnter() {
			_flgTitleSreenPrefabObject = GameObject.Instantiate (Resources.Load (ResourceDefs.kFLGScreen)) as GameObject;
		}

		public void OnExit() {
			GameObject.DestroyImmediate (_flgTitleSreenPrefabObject);
		}

		public bool CanTransition() {
			if (_timeElapsed > _maxTimeOnScreen) {
				return true;
			} else
				return false;
		}

		public IState NextState() {
			return new TitleState ();
		}

		// IUIState
		public void AnimateOn() {
			
		}
		
		public void AnimateOff() {
			
		}

		// IUpdateable
		public void Update() {
			_timeElapsed += Time.deltaTime;

			if (Input.GetMouseButtonDown (0)) {
				_timeElapsed = 10f;
			}
		}

		public void LateUpdate() {}
		public void FixedUpdate() {}

		#endregion

	}
}