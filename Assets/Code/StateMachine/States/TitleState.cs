﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using TapRPG.UI;
using UnityEngine.Advertisements;

namespace TapRPG.Core.States {
	public class TitleState : IState, IUIState
	{
		#region private variables

		private IState _nextState = default(IState);

		private GameObject _titleScreenPrefabObject;
		private TitleScreenUI _titleScreenUIScript;

		private LevelBuilder _levelBuilder;

		#endregion

		#region State Implementation
		
		// IState
		public void OnEnter() {
			_titleScreenPrefabObject = GameObject.Instantiate (Resources.Load (ResourceDefs.kTitleScreen)) as GameObject;
			_titleScreenUIScript = _titleScreenPrefabObject.GetComponent<TitleScreenUI> ();

			GameObject levelBuilder = GameObject.Instantiate (Resources.Load(ResourceDefs.kGAME_LevelBuilderPrefab)) as GameObject;
			_levelBuilder = levelBuilder.GetComponent<LevelBuilder> ();
			_levelBuilder.CreateLevel (EnvType.Ground_Grass_A, LevelDifficulty.Easy);

			// Attach hanlders
			ConfigerTitleHandlers (true);
		}
		
		public void OnExit() {

			// Detach handlers
			ConfigerTitleHandlers (false);

			GameObject.DestroyImmediate (_titleScreenPrefabObject);
			GameObject.DestroyImmediate (_levelBuilder.gameObject);
		}

		public bool CanTransition() {
			return _nextState != default(IState);
		}
		
		public IState NextState() {
			return _nextState;
		}
		
		// IUIState
		public void AnimateOn() {}
		
		public void AnimateOff() {}
		
		#endregion

		#region Button Handlers

		private void ConfigerTitleHandlers (bool toggle) {
			if (toggle) {
				_titleScreenUIScript.TitlePressedEvent 				+= HandleTitlePressedEvent;
				_titleScreenUIScript.PlayGameButtonPressedEvent 	+= HandlePlayGameButtonPressedEvent;
				_titleScreenUIScript.OptionsButtonPressedEvent 		+= HandleOptionsButtonPressedEvent;
			} else {
				_titleScreenUIScript.TitlePressedEvent 				-= HandleTitlePressedEvent;
				_titleScreenUIScript.PlayGameButtonPressedEvent 	-= HandlePlayGameButtonPressedEvent;
				_titleScreenUIScript.OptionsButtonPressedEvent 		-= HandleOptionsButtonPressedEvent;
			}
		}

		void HandleTitlePressedEvent ()
		{
			_levelBuilder.ShuffleLevel ();
		}

		void HandleOptionsButtonPressedEvent ()
		{
			Debug.Log ("<color=red> Need Options Button Method </color>");
		}

		void HandlePlayGameButtonPressedEvent ()
		{
			_nextState = new GameState ();
		}

		#endregion
	}
}
