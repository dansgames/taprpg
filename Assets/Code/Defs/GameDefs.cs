﻿public static class GameDefs {

	public const int kGameBoardWidth 	= 11;
	public const int kGameBoardHeight 	= 14;

	public const int kBlockSize			= 1;

	public const float kDefaultRunSpeed	= 0.15f;
}
