﻿public static class ResourceDefs {

	// Game
	private const string kGameResources 			= "Game/";
	public const string kGAME_LevelBuilderPrefab 	= kGameResources + "LevelBuilder";
	public const string kGAME_PlayerSuit			= kGameResources + "Player/";

	// Levels & Ground
	private const string kGroundResources 		= "Ground/";
	public const string kEnvGroundResources		= kGroundResources + "Env/";
	public const string kGroundTraps			= kGroundResources + "Trap/";

	// Items
	private const string kItemResources			= "Item/";
	public const string kITEM_Collect			= kItemResources + "Collectable/";

	// Enemies
	private const string kEnemyResources		= "Enemy/";
	public const string kENEMY_Slime			= kEnemyResources + "Slime";

	// Screens
	private const string kScreenResources		= "Screen/";
	public const string kFLGScreen				= kScreenResources + "scrFLG";
	public const string kTitleScreen			= kScreenResources + "scrTitle";

	// GUI Elements
	private const string kGUIResources			= kScreenResources + "HUD/";
	public const string kGameHUD				= kGUIResources + "GameHUD";
	public const string kRewardHUD				= kGUIResources + "RewardHUD";
}
