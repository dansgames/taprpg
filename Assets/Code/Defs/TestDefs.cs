﻿public static class TestDefs{

	// Test flags, false on shipped versions
	public const bool 				kBuildTestLevelOnStart 					= true;

	// Test Settings
	public const EnvType 			kTestLevelType 							= EnvType.Ground_Grass_A;
	public const LevelDifficulty 	kTestLevelDifficulty 					= LevelDifficulty.Easy;
}
