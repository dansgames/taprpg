﻿public enum LevelDifficulty {
	Easy,
	Medium,
	Hard,
	KillMeNow
}

public enum EnvType {
	Ground_Grass_A,
}

public enum TrapType {
	Ground_Trap_Lava,
}

public enum ItemType {
	Item_Star,
}

public enum EnemyType {
	Slime,
}