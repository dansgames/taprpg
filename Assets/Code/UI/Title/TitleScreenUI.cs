﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace TapRPG.UI {

	public class TitleScreenUI : AbstractUIScreen {

		#region events

		public delegate void ButtonPressEventHandler();
		public event ButtonPressEventHandler TitlePressedEvent;
		public event ButtonPressEventHandler PlayGameButtonPressedEvent;
		public event ButtonPressEventHandler OptionsButtonPressedEvent;

		#endregion

		#region private fields

		private Canvas _myScreenCanvas;
		
		private Button _btnGameOne;
		private Button _btnGameTwo;
		private Button _btnGameThree;

		private Button _btnOptions;

		private Button _btnRemoveAdvs;
		private Button _btnGameCenter;
		private Button _btnRating;

		private Button _btnTitle;

		#endregion

		#region MonoBehaviour inherited

		void Awake() {
			// Options
			_btnOptions = transform.FindChild ("btnOptions").GetComponent<Button> ();
			_btnOptions.onClick.AddListener (OptionsClicked);

			// Play Game
			_btnGameOne = transform.FindChild ("btnPlayGame").GetComponent<Button> ();
			_btnGameOne.onClick.AddListener (PlayGameClick);

			// Title (level shuffle)
			_btnTitle = transform.FindChild ("btnTapTitle").GetComponent<Button> ();
			_btnTitle.onClick.AddListener (TitleClick);
		}

		#endregion

		#region private methods

		private void PlayGameClick() {
			if (PlayGameButtonPressedEvent != null)
				PlayGameButtonPressedEvent ();
		}

		private void TitleClick() {
			if (TitlePressedEvent != null)
				TitlePressedEvent ();
		}

		private void OptionsClicked() {
			if (OptionsButtonPressedEvent != null)
				OptionsButtonPressedEvent ();
		}

		#endregion
	}
}