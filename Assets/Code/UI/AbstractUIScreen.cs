﻿using UnityEngine;
using System.Collections;

namespace TapRPG.UI {

	public abstract class AbstractUIScreen : MonoBehaviour {

		#region UI Screen Events

		public delegate void UIScreenAnimationEventHandler();
		public event UIScreenAnimationEventHandler AnimateOnCompleteEvent;
		public event UIScreenAnimationEventHandler AnimateOffCompleteEvent;

		#endregion

		public virtual void AnimateOn() {

		}

		public virtual void AnimateOnComplete() {
			if (AnimateOnCompleteEvent != null)
				AnimateOnCompleteEvent ();
		}

		public virtual void AnimateOff() {

		}

		public virtual void AnimateOffComplete() {
			if (AnimateOffCompleteEvent != null)
				AnimateOffCompleteEvent ();
		}
	}
}