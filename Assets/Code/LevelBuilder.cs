﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelBuilder : MonoBehaviour {

	#region private fields

	private Transform _anchor;
	private Dictionary<Vector2, bool> _freeBlocks;
	private List<GameObject> _currentBlocks = new List<GameObject>();
	private List<GameObject> _oldBlocks = new List<GameObject>();

	#endregion

	#region MonoBehaviour Inherited

	void Awake() {
		_anchor = transform.FindChild ("Anchor");
	}

	#endregion

	#region public methods

	public void CreateLevel(EnvType type, LevelDifficulty difficulty) {
		_freeBlocks = new Dictionary<Vector2, bool> ();
		GenerateGroundCubes (type);
	}

	public void ShuffleLevel() {
		_oldBlocks.AddRange (_currentBlocks);
		_currentBlocks.Clear ();
		_freeBlocks.Clear ();

		CreateLevel (EnvType.Ground_Grass_A, LevelDifficulty.Easy);

		foreach (var b in _oldBlocks) {
			GameObject.DestroyImmediate(b);
		}

		_oldBlocks.Clear ();
	}

	#endregion

	#region private methods

	/// <summary>
	/// Generates the ground cubes. Based on GameBoard size in Gamdefs
	/// </summary>
	/// <param name="type">Type.</param>
	private void GenerateGroundCubes(EnvType type) {
		
		GameObject resourceblock = GetResourceBlockForLEvelType (type) as GameObject;
		
		for (int ih = 0; ih < GameDefs.kGameBoardHeight; ih++) {
			
			for (int iw = 0; iw < GameDefs.kGameBoardWidth; iw++) {
				
				Vector3 pos = new Vector3 (-((GameDefs.kGameBoardWidth / 2) - (GameDefs.kBlockSize/2)) + iw, -10, -((GameDefs.kGameBoardHeight / 2) - (GameDefs.kBlockSize/2)) + ih);
				
				GameObject nextBlock = GameObject.Instantiate (resourceblock);
				_currentBlocks.Add(nextBlock);
				nextBlock.GetComponent<MeshRenderer>().material = GameObject.Instantiate(nextBlock.GetComponent<MeshRenderer>().material) as Material;
				nextBlock.transform.parent = _anchor;
				nextBlock.transform.localPosition = pos;

				_freeBlocks.Add (new Vector2 (pos.x, pos.z), true);

				pos.y = 0;
				
				Go.to (nextBlock.transform, Random.Range (0.3f, 0.99f), new GoTweenConfig ()
				       .localPosition(pos, false)
				       .setEaseType(GoEaseType.BackOut)
				       );
			}
		}
		
	}

	private object GetResourceBlockForLEvelType (EnvType type) {
		return Resources.Load(ResourceDefs.kEnvGroundResources + type.ToString());
	}

	private void SpawnStarAt(Vector2 pos) {
		GameObject star = GameObject.Instantiate (Resources.Load (ResourceDefs.kITEM_Collect + ItemType.Item_Star.ToString ())) as GameObject;
		star.transform.position = new Vector3 (pos.x, 1f, pos.y);
	}

	#endregion
}
