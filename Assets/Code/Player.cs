﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	#region private variables

	private bool _bSHouldBeMoving = false;
	private bool _bCanMove = false;

	private float _runSpeed = 0.15f;

	private bool _bDead = false;

	#endregion

	// Update is called once per frame
	void LateUpdate () {

		if (Input.GetMouseButtonDown (0) && _bSHouldBeMoving && _bCanMove) {
			transform.RotateAround (transform.localPosition, Vector3.up, 90f);
		}

		if (_bSHouldBeMoving && !_bCanMove && Input.GetMouseButtonDown (0)) {
			_bCanMove = true;
		}

		if (_bSHouldBeMoving && _bCanMove) {
			transform.Translate(Vector3.forward*_runSpeed);
			Vector3 newPos = transform.position;

			// Old come in other side of screen
//			if(newPos.x > 5.5f) newPos.x = -5.5f;
//			if(newPos.x < -5.5f) newPos.x = 5.5f;
//			if(newPos.z > 6.5f) newPos.z = -7.5f;
//			if(newPos.z < -7.5f) newPos.z = 6.5f;

			if(newPos.x > 5.5f) 	_bDead = true;
			if(newPos.x < -5.5f) 	_bDead = true;
			if(newPos.z > 6.5f) 	_bDead = true;
			if(newPos.z < -7.5f) 	_bDead = true;

			newPos.y = (Mathf.Sin (Time.time*25f)+1)/5f;
			transform.position = newPos;
		}
	}

	#region public interface methods

	public void SetRunSpeed(float speed) {
		_runSpeed = speed;
	}

	public void ToggleMove(bool inState) {
		_bSHouldBeMoving = inState;
	}

	public bool IsDead() {
		return _bDead;
	}

	#endregion
}
