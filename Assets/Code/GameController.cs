﻿using UnityEngine;
using System.Collections;

using TapRPG.Core;
using TapRPG.Core.States;

public sealed class GameController : MonoBehaviour {

	#region private vairables

	private StateMachine _gameStateMachine;
	private StateMachine _uiStateMachine;

	#endregion

	#region Monobehaviour Inherited

	void Awake() {
		_gameStateMachine = gameObject.AddComponent<StateMachine>();
		_gameStateMachine.ChangeState (new BeginState ());
	}


	#endregion
}
