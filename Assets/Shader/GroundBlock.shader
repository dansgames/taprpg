﻿Shader "TapRPG/GroundBlock" {
    Properties {
        _Color ("Bottom Color", Color) = (1,1,1,1)
	    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
    }
    SubShader {
        Pass {
        	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        	
        	ZWrite Off
        	Blend SrcAlpha OneMinusSrcAlpha 
        	
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            
            struct a2v {
	        	float4 vertex : POSITION;
	        	float2 texcoord : TEXCOORD0;
			};
            
            struct v2f {
	        	float4 vertex : SV_POSITION;
	        	half2 texcoord : TEXCOORD0;
	        	float posY : Float;
	        };
	 
	        sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Color;

            v2f vert (a2v v)
            {
                v2f o;
                o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                
                o.posY = v.vertex.y;
           		
                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex); // Get Texture Coordinate from Main Tex

                return o;
            }
	
            fixed4 frag (v2f i) : COLOR
            {
				fixed4 col = tex2D(_MainTex, i.texcoord) * _Color;
				col.a = lerp(0, 2, i.posY);
                return col;
            }
            ENDCG
        }
    }
}