Shader "George/Vertex Multi Color By World Vertices With Alpha" {
	Properties {
	    _Color ("Bottom Color", Color) = (1,1,1,1)
	    _Color2 ("Top Color", Color) = (1,1,1,1)
	    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	    _Scale ("Objects Real World Size", Float) = 1
	}
	 
	SubShader {
	    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	    LOD 100
	   
	    ZWrite Off
	    Blend SrcAlpha OneMinusSrcAlpha
	   
	    Pass {
	        CGPROGRAM
	            #pragma vertex vert
	            #pragma fragment frag
	           
	            #include "UnityCG.cginc"
	           
	            struct appdata_t {
	                float4 vertex : POSITION;
	                float2 texcoord : TEXCOORD0;
	            };
	 
	            struct v2f {
	                float4 vertex : SV_POSITION;
	                half2 texcoord : TEXCOORD0;
	                fixed4 col : COLOR;
	            };
	 
	            sampler2D _MainTex;
	            float4 _MainTex_ST;
	            fixed4 _Color;
	            fixed4 _Color2;
	            fixed  _Scale;

	            v2f vert (appdata_t v)
	            {
	                v2f o;
	                o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
	                
	                float3 worldPos = mul (_Object2World, v.vertex).xyz;
	                float pos = min( 1, worldPos.y * _Scale );
	                pos = max( 0, pos );
	                o.col = lerp(_Color,_Color2, pos);
	                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

	                return o;
	            }
	           
	            fixed4 frag (v2f i) : SV_Target
	            {
					fixed4 col = tex2D(_MainTex, i.texcoord) * i.col;
	                return col;
	            }
	        ENDCG
	    }
	}
 
}
 