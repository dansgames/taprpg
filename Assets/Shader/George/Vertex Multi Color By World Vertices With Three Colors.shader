Shader "George/Vertex Multi Color By World Vertices With Three Colors" {
	Properties {
	    _Color ("Bottom Color", Color) = (1,1,1,1)
	   	_Color2 ("Mid Color", Color) = (1,1,1,1)
	    _Color3 ("Top Color", Color) = (1,1,1,1)
	    
	    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	    _Scale ("Objects Real World Size", Float) = 1
	}
	 
	SubShader {
	    Tags {"Queue"="Geometry"}
	    LOD 100
	   
	    Blend SrcAlpha OneMinusSrcAlpha
	   
	    Pass {
	        CGPROGRAM
	            #pragma vertex vert
	            #pragma fragment frag
	           
	            #include "UnityCG.cginc"
            	
	            struct appdata_t {
	                float4 pos : POSITION;
	                float2 texcoord : TEXCOORD0;
	            };
	 
	            struct v2f {
	                float4 pos : SV_POSITION;
	                half2 texcoord : TEXCOORD0;
	                fixed4 col : COLOR;
	            };
	 
	            sampler2D _MainTex;
	            float4 _MainTex_ST;
	            fixed4 _Color;
	            fixed4 _Color2;
	            fixed4 _Color3;
	            fixed  _Scale;

	            v2f vert (appdata_t v)
	            {
	                v2f o;
	                o.pos = mul(UNITY_MATRIX_MVP, v.pos);
	                
	                float3 worldPos = mul (_Object2World, v.pos).xyz;
	                float pos = min( 1, worldPos.y * _Scale );
	                pos = max( 0, pos );
	                
	                if(pos < 0.5)
	                {
	                	o.col = lerp(_Color,_Color2, pos * 2);
	                }
	                else
	                {
	                	o.col = lerp(_Color2,_Color3, (pos - 0.5) * 2);
	                }
	                
	                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

	                return o;
	            }
	           
	            fixed4 frag (v2f i) : SV_Target
	            {
					fixed4 col = tex2D(_MainTex, i.texcoord) * i.col;
	                return col;
	            }
	        ENDCG
	    }
	}
 
     Fallback "VertexLit"

}
 