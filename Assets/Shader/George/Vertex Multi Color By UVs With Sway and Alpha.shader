﻿Shader "George/Vertex Multi Color By UVs With Sway With Alpha" {
	Properties {
	    _Color ("Bottom Color", Color) = (1,1,1,1)
	    _Color2 ("Mid Color", Color) = (1,1,1,1)
	   	_Color3 ("Top Color", Color) = (1,1,1,1)
	   	_Midpoint ("Midpoint", Float) = 0.5 
	    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	    
	    _Cutoff ("Cutoff", Float) = 0.05

	   	_WaveDistance ("Wave Distance", Float) = 1
	   	_SinFactor ("Sin Factor", Float) = 50
	}
	 
	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	    LOD 100
	   
	   
	   	ZWrite Off
	   	Blend SrcAlpha OneMinusSrcAlpha

	    Pass {
	        CGPROGRAM
	            #pragma vertex vert
	            #pragma fragment frag
	           
	            #include "UnityCG.cginc"
	           
	            struct appdata_t {
	                float4 vertex : POSITION;
	                float2 texcoord : TEXCOORD0;
	            };
	 
	            struct v2f {
	                float4 vertex : SV_POSITION;
	                half2 texcoord : TEXCOORD0;
	                fixed4 col : COLOR;
	            };
	 
	            sampler2D _MainTex;
	            float4 _MainTex_ST;
	            fixed4 _Color;
	            fixed4 _Color2;
	            fixed4 _Color3;

	            fixed  _Midpoint;

	            fixed  _WaveDistance;
	            fixed  _SinFactor;

	            v2f vert (appdata_t v)
	            {
	                v2f o;
	                
	                if(v.texcoord.y < _Midpoint)
	                {
	                	o.col = lerp(_Color,_Color2, v.texcoord.y * (1.0/_Midpoint));
	                }
	                else
	                {
	                	o.col = lerp(_Color2,_Color3, (v.texcoord.y - _Midpoint) * (1.0/ (1.0-_Midpoint)));
	                }

	            	o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
	                o.vertex.x += sin(_Time * _SinFactor) * v.texcoord.y * _WaveDistance;
	                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

	                return o;
	            }
	           
	            fixed4 frag (v2f i) : SV_Target
	            {
					fixed4 col = tex2D(_MainTex, i.texcoord) * i.col;
	                return col;
	            }
	        ENDCG
	    }
	    
	    Pass {
			Name "Caster"
			Tags { "LightMode" = "ShadowCaster" }
			Offset 1, 1
			
			Fog {Mode Off}
			ZWrite On ZTest LEqual Cull Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_shadowcaster
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform fixed _Cutoff;
			uniform fixed4 _Color;

	            fixed  _WaveDistance;
	            fixed  _SinFactor;

	            struct v2f {
	            				V2F_SHADOW_CASTER;

	                float4 vertex : SV_POSITION;
	                half2 texcoord : TEXCOORD0;
	                fixed4 col : COLOR;
	            };
			uniform float4 _MainTex_ST;

			v2f vert( appdata_base v )
			{
				v2f o;
				TRANSFER_SHADOW_CASTER(o)
				
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
	            o.vertex.y += sin(_Time * _SinFactor) * v.texcoord.y * _WaveDistance / 4;
	            o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
	            
				return o;
			}
	            
			float4 frag( v2f i ) : COLOR
			{
				fixed4 texcol = tex2D( _MainTex, i.texcoord );
				clip( texcol.a*_Color.a - _Cutoff );
				
				SHADOW_CASTER_FRAGMENT(i)
			}
			ENDCG
		}
 	}
}
 